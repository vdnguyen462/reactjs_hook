import logo from './logo.svg';
import './App.css';
import ShoeShopHook from './ShoeShopHook/ShoeShopHook';
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import Header from './Components/Header/Header';
import ShoeShopHookRedux from './ShoeShopHookRedux/ShoeShopHookRedux';

function App() {
  return (
    <div>
      <BrowserRouter>
      <Header/>
        <Routes>
          <Route path='/' element={<ShoeShopHook/>}/>
          <Route path='/shoe-hook-redux' element={<ShoeShopHookRedux/>}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
