import React from 'react'

export default function Cart(props) {
  let { cart, handleChangeQuantity, handleDeleteShoe } = props;
  let renderTBody = () => { 
    return cart.map((item) => { 
      return (
        <tr className='text-center'>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <button 
            onClick={() => { 
              handleChangeQuantity(item.id, -1)
            }}
            className='btn btn-danger'>-</button>
            <span className='mx-3'>{item.soLuong}</span>
            <button 
            onClick={() => { 
              handleChangeQuantity(item.id, 1)
             }}
            className='btn btn-success'>+</button>
          </td>
          <td>{item.price * item.soLuong}$</td>
          <td><img src={item.image} style={{width: "3rem"}}/></td>
          <td>
            <button 
            onClick={() => { 
              handleDeleteShoe(item.id)
             }}
            className='btn btn-danger'>Delete</button>
          </td>
        </tr>
      )
     })
   }
  return (
    <div>
      <table className="table">
        <thead className='text-center'>
          <td>ID</td>
          <td>Name</td>
          <td>Quality</td>
          <td>Price</td>
          <td>Image</td>
          <td>Action</td>
        </thead>
        <tbody>
          {renderTBody()}
        </tbody>
      </table>
    </div>
  )
}
