import React, { useState } from 'react'
import Cart from './Cart';
import { dataShoe } from './DataShoe'
import ListShoe from './ListShoe';

export default function ShoeShopHook() {
  let [list, setList] = useState(dataShoe);
  let [cart, setCart] = useState([]);

  let handleAddToCart = (shoe) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    })
    if(index == -1){
      let newShoe = {...shoe, soLuong:1};
      cloneCart.push(newShoe);
    }else{
      cloneCart[index].soLuong++;
    }
    setCart(cart=cloneCart);
  }

  let handleChangeQuantity = (id, number) => { 
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == id;
    })
    cloneCart[index].soLuong+=number;
    if(cloneCart[index].soLuong==0){
      cloneCart.splice(index,1);
    }
    setCart(cart=cloneCart);
   }

  let handleDeleteShoe = (id) => { 
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == id;
    })
    cloneCart.splice(index, 1);
    setCart(cart=cloneCart);
   }
  return (
    <div className='container'>
      <h2 className='text-center'>Cyber Shoe Shop</h2>
      <Cart 
      handleDeleteShoe = {handleDeleteShoe}
      handleChangeQuantity = {handleChangeQuantity}
      cart = {cart}/>
      <ListShoe
      handleAddToCart = {handleAddToCart}
      list = {list}/>
    </div>
  )
}
