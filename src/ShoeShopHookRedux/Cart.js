import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { decreaseItemAction, deleteItemAction, increaseItemAction } from './Redux/Action/ShoeAction';

export default function Cart() {
    let cart = useSelector(state => state.shoeReducer.cart);
    let dispatch = useDispatch();
    let dispatchDecreaseQuantity = (id) => dispatch(decreaseItemAction(id));
    let dispatchIncreaQuantiy = (id) => dispatch(increaseItemAction(id));
    let dispatchDeleteItem = (id) => dispatch(deleteItemAction(id));
    
    let renderTbody = () => { 
        return cart.map((item) => { 
            return (
              <tr className='text-center'>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>
                  <button 
                  onClick={() => { 
                    dispatchDecreaseQuantity(item.id)
                  }}
                  className='btn btn-danger'>-</button>
                  <span className='mx-3'>{item.soLuong}</span>
                  <button 
                  onClick={() => { 
                    dispatchIncreaQuantiy(item.id)
                   }}
                  className='btn btn-success'>+</button>
                </td>
                <td>{item.price * item.soLuong}$</td>
                <td><img src={item.image} style={{width: "3rem"}}/></td>
                <td>
                  <button 
                  onClick={() => { 
                    dispatchDeleteItem(item.id)
                   }}
                  className='btn btn-danger'>Delete</button>
                </td>
              </tr>
            )
           })
     }

  return (
    <div>
        <table className="table">
            <thead>
                <th>ID</th>
                <th>Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Image</th>
                <th>Action</th>
            </thead>
            <tbody>
                {renderTbody()}
            </tbody>
        </table>
    </div>
  )
}
