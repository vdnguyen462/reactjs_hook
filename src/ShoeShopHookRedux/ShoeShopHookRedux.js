import React from 'react'
import Cart from './Cart';
import ListShoe from './ListShoe';

export default function ShoeShopHookRedux() {
  return (
    <div className='container'>
      <h2 className='text-center'>Cyber Shoe Shop</h2>
      <Cart/>
      <ListShoe/>
    </div>
  )
}
