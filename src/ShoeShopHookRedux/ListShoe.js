import React from 'react'
import { connect, useSelector } from 'react-redux'
import { dataShoe } from './DataShoe'
import ItemShoe from './ItemShoe'


export default function ListShoe() {
    let list = useSelector(state => state.shoeReducer.listShoe);
  return (
    <div className='row'>
        {list.map((shoe) => { 
            return <ItemShoe item = {shoe}/>
        })}
    </div>
  )
}