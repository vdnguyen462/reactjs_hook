import { dataShoe } from "../../DataShoe"
import { ADD_TO_CART, DECREASE_ITEM, DELETE_ITEM, INCREASE_ITEM } from "../Constant/ShoeConstant"

const initialState = {
    listShoe: dataShoe,
    cart: [],
}


export const shoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
  case ADD_TO_CART:{
    let cloneCart = [...state.cart];
    let index = cloneCart.findIndex((item) => {
        return item.id == payload.id;
    })
    if(index == -1){
        let newShoe = {...payload, soLuong:1};
        cloneCart.push(newShoe);
    }else{
        cloneCart[index].soLuong++;
    }
    return {...state, cart: cloneCart};
  }
  case INCREASE_ITEM:{
    let cloneCart = [...state.cart];
    let index = cloneCart.findIndex((item) => {
        return item.id == payload.id;
    })
    cloneCart[index].soLuong++;
    return {...state, cart: cloneCart};
  }
  case DECREASE_ITEM: {
    let cloneCart = [...state.cart];
    let index = cloneCart.findIndex((item) => {
        return item.id == payload.id;
    })
    cloneCart[index].soLuong--;
    if(cloneCart[index].soLuong==0){
      cloneCart.splice(index,1);
    }
    return {...state, cart: cloneCart};
  }
  case DELETE_ITEM: {
    let newCart = state.cart.filter((item) => {
      return item.id!=payload;
    })
    return {...state, cart: newCart}
  }
  default:
    return state
  }
}
