import { combineReducers } from 'redux'
import { shoeReducer } from './ShoeReducer'

export const rootReducer_ShoeShop = combineReducers({
    shoeReducer: shoeReducer,
})