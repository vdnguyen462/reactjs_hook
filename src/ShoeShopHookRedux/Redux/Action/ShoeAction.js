import { ADD_TO_CART, DECREASE_ITEM, DELETE_ITEM, INCREASE_ITEM } from "../Constant/ShoeConstant"

export const addToCartAction = (shoe) => { 
    return {
        type: ADD_TO_CART,
        payload: shoe,
    }
 }

export const increaseItemAction = (id) => { 
    return {
        type: INCREASE_ITEM,
        payload: {id},
    }
 }

export const decreaseItemAction = (id) => { 
    return {
        type: DECREASE_ITEM,
        payload: {id},
    }
 }

export const deleteItemAction = (id) => { 
    return {
        type: DELETE_ITEM,
        payload: id,
    }
 }