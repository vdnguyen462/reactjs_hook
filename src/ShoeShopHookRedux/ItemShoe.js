import React from 'react'
import { addToCartAction } from './Redux/Action/ShoeAction'
import { useDispatch } from 'react-redux'

function ItemShoe(props) {
  let { item } = props;
  let dispatch = useDispatch();
  let dispatchAddToCart =  (shoe) => dispatch(addToCartAction(shoe));

  return (
    <div className='col-3 p-4'>
        <div className="card border-dark" style={{height: "75vh"}}>
        <img className="card-img-top" src={item.image} alt />
        <div className="card-body">
            <h4 className="card-title">{item.name}</h4>
            <p className="card-text">{item.description}</p>
            <button 
            onClick={() => {
                dispatchAddToCart(item)
            }}
            className='btn btn-primary'>Add to cart</button>
        </div>
        </div>
    </div>
  )
}

export default React.memo(ItemShoe)


