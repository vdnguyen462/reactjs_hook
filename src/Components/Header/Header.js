import React from 'react'
import { NavLink } from 'react-router-dom'

export default function Header() {
  return (
    <div className='py-3 border-bottom'>
        <button className='btn btn-primary mx-5'>
          <NavLink className={"text-white"} to={"/"}> Shoe Shop Hook Props </NavLink>
        </button>

        <button className='btn btn-primary'>
          <NavLink className={"text-white"} to={"/shoe-hook-redux"}> Shoe Shop Hook Redux</NavLink>
        </button>
    </div>
  )
}

